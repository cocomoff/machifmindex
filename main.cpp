#include <bits/stdc++.h>
#include <bits/stdc++.h>
#include <sdsl/suffix_arrays.hpp>

using namespace std;
using namespace sdsl;

using FMindex = csa_wt<wt_huff<rrr_vector<63>>, 256, 512>;

int main() {
    size_t max_locations = 5;
    size_t post_context  = 10;
    size_t pre_context   = 10;
    string index_suffix  = ".fm9";
    string filename      = "machi.txt";
    // string filename      = "alice29.txt";
    string index_file    = filename + index_suffix;
    string html_file     = filename + index_suffix + ".html";
    FMindex fm_index;

    if (!load_from_file(fm_index, index_file)) {
        ifstream in(filename);
        if (!in) {
            cout << "ERROR: File " << filename << " does not exist. Exit." << endl;
            return 1;
        }
        cout << "No index " << index_file << " located. Building index now." << endl;
        construct(fm_index, filename, 1);
        store_to_file(fm_index, index_file);
        std::ofstream out(html_file);
        write_structure<HTML_FORMAT>(fm_index, out);
    }

    cout << "Index construction complete, index requires " << size_in_mega_bytes(fm_index) << " MiB." << endl;
    cout << "Input search terms and press Ctrl-D to exit." << endl;
    string prompt = "\e[0;32m>\e[0m ";
    cout << prompt;
    string query;
    while (getline(cin, query)) {
        size_t m  = query.size();
        size_t occs = sdsl::count(fm_index, query.begin(), query.end());
        cout << "# of occurrences: " << occs << endl;
        if (occs > 0) {
            cout << "Location and context of first occurrences: " << endl;
            auto locations = locate(fm_index, query.begin(), query.begin()+m);
            sort(locations.begin(), locations.end());
            for (size_t i = 0, pre_extract = pre_context, post_extract = post_context; i < min(occs, max_locations); ++i) {
                cout << setw(8) << locations[i] << ": ";
                if (pre_extract > locations[i]) {
                    pre_extract = locations[i];
                }
                if (locations[i]+m+ post_extract > fm_index.size()) {
                    post_extract = fm_index.size()-locations[i]-m;
                }
                auto s   = extract(fm_index, locations[i]-pre_extract, locations[i]+m+ post_extract-1);
                string pre = s.substr(0, pre_extract);
                s = s.substr(pre_extract);
                if (pre.find_last_of('\n') != string::npos) {
                    pre = pre.substr(pre.find_last_of('\n')+1);
                }
                cout << pre;
                cout << "\e[1;31m";
                cout << s.substr(0, m);
                cout << "\e[0m";
                string context = s.substr(m);
                cout << context.substr(0, context.find_first_of('\n')) << endl;
            }
        }
        cout << prompt;
    }
    cout << endl;
}