CXX=g++
CXXFLAGS=-std=c++11 -DNDEBUG -O3
LIBFLAGS=-lsdsl -ldivsufsort -ldivsufsort64
main: main.cpp
	$(CXX) $(CXXFLAGS) -o main $^ $(LIBFLAGS)